/*
	The template... 
*/

const template =
	`
	<div class="questions">
		<div class="question-container" v-show="index === currentQuestion">
			<h2 class="question-title">{{ index+1 }}. {{ title }}</h2>
			<div class="question-info" v-html="text"></div>
			<div class="question-text" v-html="qtext">{{ qtext }}</div>
			<form v-on:submit.prevent="onSubmit" ref="form">
				<ol class="question-answers">
					<li class="question-answer"  v-for="(answer, index) in answers">
						<div v-if="type === 'checkbox'">
							<label :for="id + index">
								<input :value="answer.id" v-model="userInput" :name="id + index" :id="id + index" type="checkbox"/>
								{{ answer.text }}
							</label>
						</div>
						<div v-if="type === 'radio'">
							<label :for="id  + index">
								<input :value="answer.id" v-model="userInput" :name="id" :id="id  + index" type="radio"/>
								{{ answer.text }}
							</label>
						</div>
					</li>
				</ol>
				<button class="button button-answer" @click.prevent="clickHandle">Submit your answer</button>
			</form>
		</div>	

		<div class="question-modal" v-if="asked">
			<div class="question-modal__cover" v-on:click="hideModal()"></div>
			<div class="question-modal__panel">
				<div class="question-feedback" v-html="feedback"></div>
				<button v-if="this.correct==false" class="button" v-on:click="hideModal()">Close</button>
				<button v-if="this.correct" class="button" v-on:click="next()">Next</button>
			</div>
		</div>
	</div>
`
/*
	this is going to be consumed as a vue component,
	so we only need to export an object... which is nice
*/

const Question = {

	/*
		the required props
	*/

	props: {
		id: {
			type: String,
			required: true
		},
		type: {
			type: String,
			required: true
		},
		answers: {
			type: Array,
			required: true
		},
		responses: {
			type: Object,
			required: true
		},
		title: {
			type: String,
			required: true
		},
		text: {
			type: String,
			required: true
		},
		qtext: {
			type: String,
			required: true
		},
		index: {
			type: Number,
			required: true
		},
		total: {
			type: Number,
			required: true
		},
	},

	/*
		some local data
	*/
	data() {
		return {
			userInput: [],
			asked: false,
			correct: false,
			prev: null,
			currentQuestion: 0
		}
	},

	/*
		create a computed property
		total, which is the total number of correct answers
	*/
	computed: {
		total() {
			return this.answers.filter((answer) => answer.correct).length
		},

		correctedIds() {
			return this.answers.filter((answer) => answer.correct).map((answer) => answer.id)
		},

		feedback() {
			const { correct, incorrect } = this.responses
			return this.correct ? correct : incorrect
		},

	},


	/*
		watch the user property.
		each input is bound to this object, so when the input changes 
		(or the value changes) this function will be trigger
	*/
	watch: {
		/*
			input is the :value from the input
		*/
		userInput(input) {

			if(!this.correct) {
				if(this.type === 'checkbox') {
					if(input.filter((id) => this.correctedIds.includes(id)).length === this.total && input.length <= this.total) {
						this.correct = true
					}
				} 

				if(this.type === 'radio') {
					if(this.correctedIds.includes(input)) {
						this.correct = true
						log('correct')
					}
				} 

			} else {
				this.correct = false
			}

			if(this.feedback) {
				this.feedback 
			}
			
			if(input.length === 0) {
				this.asked = false
			}
		}
	},


	methods: {
		clickHandle() {
			this.asked = true
		},

		onSubmit() {
			this.asked = true
		},
		
	},

	template

}

new Vue({

	/*
		le data... can't remember if it needs to be a function 
		in this instance, but it's how I role
	*/
	data() {
		return {
			currentQuestion: 0,
			questions: [
				{
					index: 0,
					id: 'q1',
					title: 'Ensuring you are a suitable assessor',
					text: '<p><b>We need to avoid any conflicts of interest.</b> So if you have any relationship with the company or venue that you would be visiting (for example, if you or a friend work there), we would ask you not to select that visit.<b>Also, you should be a typical customer</b> - so if it\'s the kind of place you wouldn\'t normally visit, this may invalidate your report</p><p>If you\'ve been there before and had a bad time but feel like giving them another chance, that\'s fine.</p>',
					qtext: 'Which of these scenarios mean there is a potential conflict of interest?',
					type: 'checkbox',
					answers: [
						{
							id: 'a1',
							text: 'I have been there before and normally enjoy it but didn\'t last time'
						},
						{
							id: 'a2',
							text: 'I work there (or have in the past) or I know someone who does',
							correct: true
						},
						{
							id: 'a3',
							text: 'I don\'t really like the style of service for that brand so wouldn\'t choose to go there ordinarily',
							correct: true
						},
						{
							id: 'a4',
							text: 'I don\'t know the location of the visit'
						}
					],
					responses: {
						correct: '<h3>That is correct, well done</h3><p>Proceed to the next question</p>',
						incorrect: '<h3>Incorrect try again</h3><p>Read the text carefully and make sure you choose 2 options</p>'
					},
				},
				{
					index: 1,
					id: 'q2',
					title: 'Booking and cancelling a visit',
					text: '<p>When you book a visit it will be for a specific date (and probably time). We need to be sure that <b>you will make every effort to complete the visit</b> and report as scheduled. Otherwise it can really mess up the performance reporting schedule for our clients.</p><p>If you come across any difficulties or need to cancel the visit, please let us know as soon as possible so that we can try to make alternative arrangements.</p>',
					qtext: 'You\'ve booked a visit but something urgent has come up which means you can\'t attend. What should you do?',
					type: 'radio',
					answers: [
						{
							id: 'a1',
							text: 'Just go the following day'
						},
						{
							id: 'a2',
							text: 'Cancel the visit online or contact FOO as soon as possible',
							correct: true
						},
						{
							id: 'a3',
							text: 'Ask a friend to go for you and make notes'
						},
						{
							id: 'a4',
							text: 'Go back online and sign up for antoher visit without cancelling this one'
						}
					],
					responses: {
						correct: 'That is correct, well done',
						incorrect: 'Incorrect, try again'
					},
				},
			]
		}
	},
	/*
		register the component
	*/
	components: {
		'question-template': Question
	},

	computed: {
		total() {
			return this.questions.length
		},
		
	},

	methods: {
		next() {
			this.hideModal()
			this.currentQuestion += 1
		},
		previous() {

		},
		hideModal() {
			this.asked = false	
		},
	},

	/*
		the template, passing some properties to the question component
		each question is a component, which is key
	*/
	template: `
		<div>
			<div v-for='(question, index) in questions'>
				<question-template 
					:id="question.id" 
					:answers='question.answers' 
					:type='question.type' 
					:responses='question.responses' 
					:title='question.title' 
					:text='question.text' 
					:qtext='question.qtext'
					:index='index'
					:total='total'
				/>
			</div>
		</div>
		
	`

}).$mount('#app')